package org.jprogger.ui;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import org.jprogger.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import uk.co.senab.photoview.PhotoView;

public class ImageDetailActivity extends AppCompatActivity {

    public static final String IMAGE_URL = "image_url";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.photo_view)
    PhotoView photoView;

    private String imageUrl;
    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.activity_image_detail);
        unbinder = ButterKnife.bind(this);

        imageUrl = (state != null) ? state.getString(IMAGE_URL) :
                getIntent().getStringExtra(IMAGE_URL);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(R.string.image_details_title);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Picasso.with(this)
                .load(Uri.parse(imageUrl))
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.error_placeholder)
                .into(photoView);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(IMAGE_URL, imageUrl);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
