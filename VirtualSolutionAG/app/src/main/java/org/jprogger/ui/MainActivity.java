package org.jprogger.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import org.jprogger.PixabayApplication;
import org.jprogger.R;
import org.jprogger.net.PixabayService;
import org.jprogger.net.model.Pixabay;
import org.jprogger.utils.Utils;
import org.jprogger.view.RecyclerAdapter;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.list_view)
    RecyclerView recyclerView;

    @BindString(R.string.pixabay_api_key)
    String pixabayApiKey;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @Inject
    PixabayService pixabayService;

    private String query;
    private Unbinder unbinder;
    private SearchView searchView;
    private RecyclerAdapter adapter;


    @Override
    protected void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.activity_main);
        PixabayApplication.getAppComponent().inject(this);
        unbinder = ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        progressBar.setVisibility(View.GONE);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setAdapter(new RecyclerAdapter(this, null));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
    }

    @Override
    protected void onResume() {
        super.onResume();
        load(query);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        MenuItem searchMenuItem = menu.findItem(R.id.search);

        searchView = (SearchView) searchMenuItem.getActionView();
        searchView.setQueryHint(getString(R.string.query_hint));
        searchView.setOnQueryTextListener(new QueryTextListener(searchView));
        searchView.setOnCloseListener(new QueryCloseListener());
        return super.onCreateOptionsMenu(menu);
    }

    private void load(String query) {
        recyclerView.setAdapter(null);
        progressBar.setVisibility(View.VISIBLE);
        Observable<Pixabay> observable = (TextUtils.isEmpty(query)) ?
                pixabayService.loadRecent(pixabayApiKey, 1) :
                pixabayService.loadWithQuery(pixabayApiKey, query, 1);

        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(getRxObserver());
    }

    private Observer<Pixabay> getRxObserver() {
        return new Observer<Pixabay>() {
            @Override
            public void onSubscribe(Disposable d) {
                Log.i(PixabayApplication.TAG, "onSubscribe called");
            }

            @Override
            public void onNext(Pixabay pixabay) {
                Log.i(PixabayApplication.TAG, "onNext called");
                adapter = new RecyclerAdapter(MainActivity.this, pixabay.hits);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onError(Throwable e) {
                Log.i(PixabayApplication.TAG, "onError called");
                Log.e(PixabayApplication.TAG, e.toString());
            }

            @Override
            public void onComplete() {
                Log.i(PixabayApplication.TAG, "onComplete called");
                progressBar.setVisibility(View.GONE);
            }
        };
    }

    private class QueryCloseListener implements SearchView.OnCloseListener {

        @Override
        public boolean onClose() {
            Log.i(PixabayApplication.TAG, "onClose search called");
            Utils.hideKeyboard(MainActivity.this, searchView);
            load(null);
            return true;
        }
    }

    private class QueryTextListener implements SearchView.OnQueryTextListener {

        private SearchView searchView;

        public QueryTextListener(SearchView searchView) {
            this.searchView = searchView;
        }

        @Override
        public boolean onQueryTextSubmit(String query) {
            Log.i(PixabayApplication.TAG, "onQuerySubmit called");
            Utils.hideKeyboard(MainActivity.this, searchView);
            MainActivity.this.query = query;
            return true;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            Log.i(PixabayApplication.TAG, "onQueryTextChange called");
            query = newText;
            load(query);
            return true;
        }
    }
}
