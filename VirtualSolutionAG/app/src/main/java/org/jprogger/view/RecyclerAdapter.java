package org.jprogger.view;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import org.jprogger.R;
import org.jprogger.net.model.Pixabay;
import org.jprogger.ui.ShowImageDialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private AppCompatActivity context;
    private Pixabay.ImageInfo[] imageInfos;

    public RecyclerAdapter(AppCompatActivity context, Pixabay.ImageInfo[] imageInfos) {
        super();
        this.context = context;
        this.imageInfos = imageInfos;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (imageInfos != null) {
            final Pixabay.ImageInfo imageInfo = imageInfos[position];
            Picasso.with(context)
                    .load(Uri.parse(imageInfo.previewURL))
                    .resizeDimen(R.dimen.preview_image_width, R.dimen.preview_image_height)
                    .error(R.drawable.error_placeholder)
                    .placeholder(R.drawable.placeholder)
                    .centerCrop()
                    .noFade()
                    .into(holder.imageView);
            holder.tagsView.setText(imageInfo.tags);
            holder.userNameView.setText(imageInfo.userName);

            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ShowImageDialogFragment dialogFragment =
                            ShowImageDialogFragment.newInstance(context.getString(R.string.dialog_title), imageInfo.webformatURL);
                    dialogFragment.show(context.getSupportFragmentManager(), "dialog_fragment");
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return imageInfos != null ? imageInfos.length : 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.card_view)
        CardView cardView;

        @BindView(R.id.image_view)
        ImageView imageView;

        @BindView(R.id.item_tags)
        AppCompatTextView tagsView;

        @BindView(R.id.pixabay_user_name)
        AppCompatTextView userNameView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
