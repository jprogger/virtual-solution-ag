package org.jprogger.utils;

import android.content.Context;
import android.os.Build;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class Utils {

    public static boolean shouldUseCompatMenu() {
        int currentApiVersion = android.os.Build.VERSION.SDK_INT;
        return currentApiVersion <= Build.VERSION_CODES.HONEYCOMB;
    }

    public static void hideKeyboard(Context context, View focusedView) {
        if (focusedView != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
        }
    }
}
