package org.jprogger.net.model;

import com.google.gson.annotations.SerializedName;

public class Pixabay {

    @SerializedName("total")
    public int total;

    @SerializedName("totalHits")
    public int totalHits;

    @SerializedName("hits")
    public ImageInfo[] hits;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getTotalHits() {
        return totalHits;
    }

    public void setTotalHits(int totalHits) {
        this.totalHits = totalHits;
    }

    public ImageInfo[] getHits() {
        return hits;
    }

    public void setHits(ImageInfo[] hits) {
        this.hits = hits;
    }

    public static class ImageInfo {

        @SerializedName("id")
        public int id;

        @SerializedName("pageURL")
        public String pageUrl;

        @SerializedName("type")
        public String type;

        @SerializedName("tags")
        public String tags;

        @SerializedName("previewURL")
        public String previewURL;

        @SerializedName("previewWidth")
        public int previewWidth;

        @SerializedName("previewHeight")
        public int previewHeight;

        @SerializedName("webformatURL")
        public String webformatURL;

        @SerializedName("webformatWidth")
        public int webformatWidth;

        @SerializedName("webformatHeight")
        public int webformatHeight;

        @SerializedName("imageSize")
        public int imageSize;

        @SerializedName("views")
        public int views;

        @SerializedName("downloads")
        public int downloads;

        @SerializedName("favorites")
        public int favorites;

        @SerializedName("likes")
        public int likes;

        @SerializedName("comments")
        public int comments;

        @SerializedName("user_id")
        public int userId;

        @SerializedName("user")
        public String userName;

        @SerializedName("userImageURL")
        public String userImageURL;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getPageUrl() {
            return pageUrl;
        }

        public void setPageUrl(String pageUrl) {
            this.pageUrl = pageUrl;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getTags() {
            return tags;
        }

        public void setTags(String tags) {
            this.tags = tags;
        }

        public String getPreviewURL() {
            return previewURL;
        }

        public void setPreviewURL(String previewURL) {
            this.previewURL = previewURL;
        }

        public int getPreviewWidth() {
            return previewWidth;
        }

        public void setPreviewWidth(int previewWidth) {
            this.previewWidth = previewWidth;
        }

        public int getPreviewHeight() {
            return previewHeight;
        }

        public void setPreviewHeight(int previewHeight) {
            this.previewHeight = previewHeight;
        }

        public String getWebformatURL() {
            return webformatURL;
        }

        public void setWebformatURL(String webformatURL) {
            this.webformatURL = webformatURL;
        }

        public int getWebformatWidth() {
            return webformatWidth;
        }

        public void setWebformatWidth(int webformatWidth) {
            this.webformatWidth = webformatWidth;
        }

        public int getWebformatHeight() {
            return webformatHeight;
        }

        public void setWebformatHeight(int webformatHeight) {
            this.webformatHeight = webformatHeight;
        }

        public int getImageSize() {
            return imageSize;
        }

        public void setImageSize(int imageSize) {
            this.imageSize = imageSize;
        }

        public int getViews() {
            return views;
        }

        public void setViews(int views) {
            this.views = views;
        }

        public int getDownloads() {
            return downloads;
        }

        public void setDownloads(int downloads) {
            this.downloads = downloads;
        }

        public int getFavorites() {
            return favorites;
        }

        public void setFavorites(int favorites) {
            this.favorites = favorites;
        }

        public int getLikes() {
            return likes;
        }

        public void setLikes(int likes) {
            this.likes = likes;
        }

        public int getComments() {
            return comments;
        }

        public void setComments(int comments) {
            this.comments = comments;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getUserImageURL() {
            return userImageURL;
        }

        public void setUserImageURL(String userImageURL) {
            this.userImageURL = userImageURL;
        }
    }
}
