package org.jprogger.net;

import org.jprogger.net.model.Pixabay;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PixabayService {

    String API_ENDPOINT = "https://pixabay.com/";

    String PARAM_KEY = "key";
    String PARAM_QUERY = "q";
    String PARAM_PAGE = "page";

    @GET("api/")
    Observable<Pixabay> loadRecent(@Query(value = PARAM_KEY, encoded = true) String apiKey, @Query(PARAM_PAGE) int pageNumber);

    @GET("api/")
    Observable<Pixabay> loadWithQuery(@Query(value = PARAM_KEY, encoded = true) String apiKey, @Query(PARAM_QUERY) String query,
                                      @Query(PARAM_PAGE) int pageNumber);
}
