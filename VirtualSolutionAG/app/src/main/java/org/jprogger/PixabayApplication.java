package org.jprogger;

import android.app.Application;

import org.jprogger.di.AppComponent;
import org.jprogger.di.AppModule;
import org.jprogger.di.DaggerAppComponent;

public class PixabayApplication extends Application {

    public static final String TAG = "Pixabay";

    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this)).build();
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }
}
